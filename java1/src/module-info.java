class Vehicle{
		void noOfWheels() {
			System.out.println("No of wheels undefined");
		}
	}
		class Scooter extends Vehicle{
			void noOfWheels() {
				System.out.println("No of wheels 2");
			}
			
		}
		class Car extends Vehicle{
			void noOfWheels() {
				System.out.println("No of wheels 4");
			}
		
}
class Main {

  public static void main(String[] args) {

  Vehicle vehicle = new Vehicle();  // Create a Vehicle object

  Vehicle scooter = new Scooter();  // Create a Scooter object

  Vehicle car = new Car();  // Create a Car object

              vehicle.noOfWheels();     //call noOfWheels of vehicle class

              scooter.noOfWheels();   //call noOfWheels of Scooter class

              car.noOfWheels();         // call noOfWheels of Car class

  }
}
